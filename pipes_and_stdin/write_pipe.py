# write_to_pipe_buf_1.py
import time
message = "hello\n"

with open("my_pipe", "w", 1) as f:
    print ("have opened pipe, commencing writing....")
    for c in message:
        f.write(c)
        print ("have sent a letter")
        time.sleep(1)
