## Example of Threading like client-server using Queue
[Link source](200~https://lyceum-allotments.github.io/2017/03/python-and-pipes-part-6-multiple-subprocesses-and-pipes/)

We need to create two FIFO files with:
```bash
mkfifo proc_a_input
mkfifo proc_b_input
```
